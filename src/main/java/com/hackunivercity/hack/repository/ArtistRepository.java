package com.hackunivercity.hack.repository;

import com.hackunivercity.hack.entity.Artist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ArtistRepository extends CrudRepository<Artist, Long> {
    Artist findById(long id);
}
