package com.hackunivercity.hack.configuration;

import com.google.gson.Gson;
import com.hackunivercity.hack.dto.ChosenTracksDTO;
import com.hackunivercity.hack.dto.LotteryDTO;
import com.hackunivercity.hack.service.StatisticService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Slf4j
public class WebSocketHandler extends TextWebSocketHandler {
    private AtomicBoolean isTaskActive = new AtomicBoolean(false);

    private ChosenTracksDTO currentTracks;//fixme если не проинициализирован

    private List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

    public void setCurrentTracksAndConvertTime(ChosenTracksDTO currentTracks) {
        this.currentTracks = currentTracks;
        log.info("got " + currentTracks.getTimer() + "secs. Converting...");
        this.currentTracks.setTimer(System.currentTimeMillis() + currentTracks.getTimer() * 1000);
        log.info("returning " + this.currentTracks.getTimer());
        isTaskActive.set(true);
    }

    @Scheduled(fixedRate = 1000)
    public void sendStatistics() throws IOException {
        if (!StatisticService.statistics.isEmpty()) {//не пустая статистика
            log.info("statistics not empty");
            if (isTaskActive.get()) { // еще можно слать
                log.info("task active" + isTaskActive);
                broadcastStatistics(); // шлем
                if (System.currentTimeMillis() - currentTracks.getTimer() >= 0) { // если время вышло
                    if (isTaskActive.get()) { // в предыдущем не вышло, шлем статистику
                        broadcastStatistics();
                    }
                    isTaskActive.set(false); //время вышло
                    log.info("finished sending statistics");
                    StatisticService.statistics.clear(); //чстим
                    currentTracks = null;
                }
            }
        }

    }

    private void broadcastStatistics() throws IOException {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("type", "poll");
        hashMap.put("choice", StatisticService.statistics);
        hashMap.put("total", StatisticService.total);
        for (WebSocketSession wss : sessions) {
            wss.sendMessage(new TextMessage(
                    new Gson().toJson(hashMap)
            ));
            log.info("send statistics = " + new Gson().toJson(hashMap) + " and session = " + wss.getId());
        }
    }

    public void broadcastCurrentTracks() throws IOException {
        log.info("broadcasting tracks");
        for (WebSocketSession wss : sessions) {
            wss.sendMessage(new TextMessage(
                    new Gson().toJson(currentTracks)
            ));
            log.info("send tracks = " + new Gson().toJson(currentTracks) + " and session = " + wss.getId());
        }
        log.info("finished broadcasting tracks");
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {
        log.info("got session " + session.getId());
        sessions.add(session);
        if (currentTracks != null) {
            session.sendMessage(new TextMessage(
                    new Gson().toJson(currentTracks)
            ));
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        log.info("removing session " + session.getId());
        sessions.remove(session);
    }

    public void startLottery() throws IOException{
        if (!sessions.isEmpty()){
            WebSocketSession winner = sessions.get(new Random().nextInt(sessions.size()));
            winner.sendMessage(new TextMessage(
                    new Gson().toJson(new LotteryDTO("lottery", "Поздравляю, вы выиграли!"))
            ));
        }
    }
}
