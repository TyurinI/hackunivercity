package com.hackunivercity.hack.controller;

import com.hackunivercity.hack.configuration.WebSocketHandler;
import com.hackunivercity.hack.dto.ChosenTracksDTO;
import com.hackunivercity.hack.dto.LoginDTO;
import com.hackunivercity.hack.model.Track;
import com.hackunivercity.hack.service.ArtistManagerService;
import com.hackunivercity.hack.service.StatisticService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@Slf4j
public class MainController {

    private StatisticService statisticService;
    private ArtistManagerService artistManagerService;
    private WebSocketHandler webSocketHandler;

    public MainController(StatisticService statisticService, ArtistManagerService artistManagerService, WebSocketHandler webSocketHandler) {
        this.statisticService = statisticService;
        this.artistManagerService = artistManagerService;
        this.webSocketHandler = webSocketHandler;
    }

    @PostMapping(value = "/getEventId")
    public Map<String, Long> getEventId(@RequestBody LoginDTO loginDTO) {
        log.info("got " + loginDTO.toString());
        long id = artistManagerService.handleArtistAndGetId(loginDTO);
        return Collections.singletonMap("eventId", id);
    }

    @GetMapping("event/{eventId}/tracks")
    public List<Track> getAllTracks(@PathVariable int eventId) {
        return artistManagerService.getTracklistToEventId(eventId);
    }

    @PostMapping("/event/{eventId}/currentTracks")
    public void getChosenTracks(@PathVariable int eventId
            , @RequestBody ChosenTracksDTO chosenTracksDTO) throws IOException {
        statisticService.initMapOfTracks(chosenTracksDTO.getTracks());
        webSocketHandler.setCurrentTracksAndConvertTime(chosenTracksDTO);
        webSocketHandler.broadcastCurrentTracks();
    }

    @GetMapping("event/{eventId}/vote/{trackId}/{strategy}/{value}")
    public void voteForSongAndUpdateInfo(@PathVariable int eventId
            , @PathVariable int trackId
            , @PathVariable String strategy
            , @PathVariable int value) {
        log.info("got event id = " + eventId);
        log.info("got track id = " + trackId);
        log.info("got value id = " + value);
        statisticService.update(trackId, strategy, value);
    }
    @GetMapping("event/{eventId}/startLottery")
    public void startLottery(@PathVariable int eventId) throws IOException{
        webSocketHandler.startLottery();
    }
}
