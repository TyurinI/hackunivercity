package com.hackunivercity.hack.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
//@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
public class Track {
    long id;
    String name;
    String title;
    int count;

    public Track(long id, String name, String title, int count) {
        this.id = id;
        this.name = name;
        this.title = title;
        this.count = count;
    }

    public Track() {
    }
}
