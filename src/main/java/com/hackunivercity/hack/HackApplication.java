package com.hackunivercity.hack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class HackApplication {

    public static void main(String[] args) {
        SpringApplication.run(HackApplication.class, args);
    }

}
