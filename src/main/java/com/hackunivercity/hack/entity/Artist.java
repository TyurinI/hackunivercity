package com.hackunivercity.hack.entity;

import com.hackunivercity.hack.dto.LoginDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "artist")
@NoArgsConstructor
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "artist", cascade = CascadeType.ALL)
    private List<Song> songs;

    public Artist(LoginDTO loginDTO) {
        this.name = loginDTO.getName();
        this.songs = new ArrayList<>();
        loginDTO.getSongs().forEach(lightSong -> {
            Song buff = new Song(lightSong.getTitle());
            buff.setArtist(this);
            songs.add(buff);
        });
    }
}
