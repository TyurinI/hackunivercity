package com.hackunivercity.hack.dto;

import com.hackunivercity.hack.model.LightSong;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class LoginDTO {
    String name;
    List<LightSong> songs;
}
