package com.hackunivercity.hack.dto;

import com.hackunivercity.hack.model.Track;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
public class ChosenTracksDTO {
    String type;
    List<Track> tracks;
    long timer;
}
