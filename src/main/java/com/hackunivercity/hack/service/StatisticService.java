package com.hackunivercity.hack.service;

import com.hackunivercity.hack.model.Track;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class StatisticService {
    public static ArrayList<StatInfo> statistics = new ArrayList<>();
    public static AtomicInteger total = new AtomicInteger(0);

    public void update(long songId, String strategy, int value) {
        for (StatInfo si : statistics) {
            if (si.getId() == songId) {
                if(strategy.equals("inc")) {
                    si.setCount(si.getCount() + value);
                    total.addAndGet(value);
                }else {
                    si.setCount(si.getCount() - 1);
                    total.decrementAndGet();
                }
            }
        }
    }


    public void initMapOfTracks(List<Track> tracks) {
        tracks.forEach(track -> statistics.add(new StatInfo(track.getId(), 0)));
        total.set(0);
    }

    @AllArgsConstructor
    @Setter
    @Getter
    private static class StatInfo {
        long id;
        int count;
    }
}
