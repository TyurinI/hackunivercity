package com.hackunivercity.hack.service;

import com.hackunivercity.hack.dto.LoginDTO;
import com.hackunivercity.hack.entity.Artist;
import com.hackunivercity.hack.model.Track;
import com.hackunivercity.hack.repository.ArtistRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class ArtistManagerService {
    ArtistRepository artistRepository;

    @Autowired
    public ArtistManagerService(ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }

    public long handleArtistAndGetId(LoginDTO loginDTO) {
        Artist artist = new Artist(loginDTO);
        log.info("got Artist " + artist.toString());
        artistRepository.save(artist);
        log.info("got Artist ID" + artist.getId());
        return artist.getId();
    }

    public List<Track> getTracklistToEventId(long eventId) {
        Artist artist = artistRepository.findById(eventId);
        ArrayList<Track> tracks = new ArrayList<>();
        artist.getSongs().forEach(sng -> {
                    tracks.add(Track.builder()
                            .id(sng.getId())
                            .name(artist.getName())
                            .title(sng.getTitle())
                            .count(sng.getCount())
                            .build()
                    );
                }
        );
        log.info(artist.getSongs().toString());
        return tracks;
    }
}
